import sys

import pandas as pd
import numpy as np


class PupilScores:
    def __init__(self, filename):
        self.scores_table = pd.read_csv(filename)

    def anyMissingData(self):
        return self.scores_table.isnull().any().any()

    def aggregation(self, subject):
        if subject not in self.scores_table:
            print ('{} is not a subject header in the pupils table'.format(subject))
            return None
        score_column = self.scores_table[subject]
        mean = score_column.mean()
        max = score_column.max()
        min = score_column.min()
        return (min, max, mean)



if __name__ == '__main__':
    score_filename = 'pupils.csv'
    scores = PupilScores(score_filename)
    print ('Loaded pupils scores from {}, it {} contains void values'.
           format(score_filename,
                  'does' if scores.anyMissingData() else 'does not'))
    for subject in ['Maths', 'Biology', 'English']:
        agg = scores.aggregation(subject=subject)
        print ('Subject: {}, Max: {}, Min: {}, Mean: {}'.
               format(subject, *agg))

    print ('bye')
    sys.exit(0)

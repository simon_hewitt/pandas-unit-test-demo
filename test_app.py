import pandas as pd
import numpy as np
import unittest
import app

class TestApp(unittest.TestCase):
    df_filename = '/tmp/test_app.csv'
    def setUp(self):
        test_data = {
            'Surname': ['Smith', 'Jones', 'Bean', 'Marx', 'Mason', 'Grashion'],
            'First Name':['Joe', 'Josie', 'Emerald', 'Peter', 'Dinah', 'Tim'],
            'Gender': ['M', 'F', 'F', 'M', 'F', 'M'],
            'Age': [11, 11, 10, 9, 10, np.NaN],
            'Maths': [88, 77, 77,88 ,np.NaN, 88],
            'Biology': [77., 88, 66, 99, 11, 99],
            'English' : [66, 99, 55, 101, np.NaN, 77]
        }

        df = pd.DataFrame(test_data)
        df.to_csv(self.df_filename)


    def test_create_class(self):
        df = app.PupilScores(self.df_filename)
        self.assertIsNotNone(df)

    def test_column_titles(self):
        df = app.PupilScores(self.df_filename).scores_table
        col_titles_set = set(df.columns.values)
        self.assertEqual(col_titles_set, {'Unnamed: 0', 'Surname', 'First Name', 'Gender', 'Age', 'Maths', 'Biology', 'English'})

    def test_aggregates(self):
        ''' PupilScores.aggregate returns (min, max, mean) for a given Subject. Calculate target from setUp
        NOTE Pandas (Numpy) excludes NaN values from the mean'''
        df = app.PupilScores(self.df_filename)
        def validate_subject_aggregate(subject, target_scores):
            test_scores = df.aggregation(subject)
            self.assertEqual(test_scores[0:2], target_scores[0:2], 'Aggregates for subject: {}'.format(subject))
            self.assertAlmostEqual(test_scores[2], target_scores[2], places=1)

        # maths 77, 88,
        validate_subject_aggregate('Maths', (77, 88, 83.6))
        validate_subject_aggregate('Biology', (11, 99, 73.333))
        validate_subject_aggregate('English', (55, 101, 79.6))

    def test_bad_subject(self):
        df = app.PupilScores(self.df_filename)
        test_score = df.aggregation('no-such-subject')
        self.assertIsNone(test_score)

    def test_no_file(self):
        with self.assertRaises(FileNotFoundError):
            app.PupilScores('no-file-here')


if __name__ == '__main__':
    unittest.main()


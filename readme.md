#Python Demo of Unit Testing and Pandas

##app.py
A simple demo of reading a CSV of pupile exam scores into a 
Pandas DataFrame and a simple method to return minimum, maximum 
and mean scores   

##test_app.py
Unit test of the class init and aggregation method, 
testing success and failure paths.

##Contact
Simon Hewitt  
simon.d.hewitt@gmail.com  
07799 381001

## Note
This took about 2 hours to create, and is intended as a simple demo of approach and 
the nature of the use of Pandas and of unit-testing